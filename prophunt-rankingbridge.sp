#include <sourcemod>
#include <sdkhooks>
#include <sdkhooks>
#include <cstrike>
#include <multicolors>
#include <smlib>
#include <prophunt>
#include <ranking>

#define PLUGIN_VERSION "1.0"
#define CHAT_PREFIX "{lime}[Ranking]{yellow} "

public Plugin myinfo = {
	name = " Prophunt - Ranking bridge",
	author = "Zipcore",
	description = "Bridge between prophunt and ranking",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net",
};

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

#define LoopArray(%1,%2) for(int %1=0;%1<GetArraySize(%2);++%1)

float g_fPoints[MAXPLAYERS + 1];

ConVar g_cvMinPlayers;

ConVar g_cvPointsDecoy;
ConVar g_cvPointsSmoke;
ConVar g_cvPointsUltimate1;
ConVar g_cvPointsUltimate2;
ConVar g_cvPointsUltimate3;
ConVar g_cvPointsHiderHitDmgDiv;
ConVar g_cvPointsHiderKill;

ConVar g_cvPointsHiderHeal;
ConVar g_cvPointsHiderMorph;
ConVar g_cvPointsHiderFreezeHeightUpgrade;
ConVar g_cvPointsHiderSpeedUpgrade;
ConVar g_cvPointsHiderLowGravityUpgrade;
ConVar g_cvPointsHiderPoisonousSmokeUpgrade;

ConVar g_cvPointsSeekerHealthshot;
ConVar g_cvPointsSeekerGrenade;
ConVar g_cvPointsSeekerFiveSeven;
ConVar g_cvPointsSeekerXM1014;
ConVar g_cvPointsSeekerMP9;
ConVar g_cvPointsSeekerM4A1;
ConVar g_cvPointsSeekerAWP;
ConVar g_cvPointsSeekerHolyLightGrenade;
ConVar g_cvPointsSeekerTauntGrenade;

ConVar g_cvPointsSeekerFail;
ConVar g_cvPointsSeekerWin;
ConVar g_cvPointsHiderSurvived;

ConVar g_cvPointsHiderDeathMin;
ConVar g_cvPointsHiderDeathPerc;

ConVar g_cvPointsSeekerDeathMin;
ConVar g_cvPointsSeekerDeathPerc;

ConVar g_cvPointsSeekerKillSteal;
ConVar g_cvPointsHiderKillSteal;

ConVar g_cvPointsHiderSmokeDmg;
ConVar g_cvPointsHiderDecoyDmg;
ConVar g_cvPointsHiderTauntTime;
ConVar g_cvPointsHiderForceTauntTime;

// fake include files :3
forward void PH_OnSmokeHitSeeker(int iTarget, int iClient, int iDmg, float fDistance, int iTargetStack, int iEntity);
forward void PH_OnDecoyHitSeeker(int iTarget, int iClient, int iDmg, float fDistance, int iEntity);

#define DEBUG false

public void OnPluginStart() 
{
	CreateConVar("prophunt_rankingbridge_version", PLUGIN_VERSION);
	
	g_cvMinPlayers = CreateConVar("prophunt_ranking_min_players", "2.0", "Min amount of players.");
	// Survive / Kill / Hit
	g_cvPointsHiderDeathMin = CreateConVar("prophunt_ranking_points_hider_death_take", "50", "Min points taken when hider dies.");
	g_cvPointsHiderDeathPerc = CreateConVar("prophunt_ranking_points_hider_death_take_perc", "0.5", "Perc points taken of your current points when a hider dies.");
	g_cvPointsHiderKillSteal = CreateConVar("prophunt_ranking_points_hider_kill_steal", "1.0", "Perc of points you can steal by killing a seeker.");
	g_cvPointsSeekerFail = CreateConVar("prophunt_ranking_points_seeker_fail", "50", "Points seekers lose when they survive a round but hiders are still alive.");
	g_cvPointsSeekerWin = CreateConVar("prophunt_ranking_points_seeker_win", "50", "Points for seekers when they survived and all hiders are dead.");
	g_cvPointsHiderSurvived = CreateConVar("prophunt_ranking_points_hider_sur", "10", "Points for hiders when they survived a round.");
	g_cvPointsHiderHitDmgDiv = CreateConVar("prophunt_ranking_points_hider_hit_dmg_div", "0.0", "Give 1 Point every X damage done to a hider by a seeker.");
	g_cvPointsHiderKill = CreateConVar("prophunt_ranking_points_hider_kill", "15", "Give points for killing a hider as seeker.");
	g_cvPointsSeekerDeathMin = CreateConVar("prophunt_ranking_points_seeker_death_take", "50", "Min points taken when seeker dies.");
	g_cvPointsSeekerDeathPerc = CreateConVar("prophunt_ranking_points_seeker_death_take_perc", "1.5", "Perc points taken of your current points when a seeker dies.");
	g_cvPointsSeekerKillSteal = CreateConVar("prophunt_ranking_points_seeker_kill_steal", "0.5", "Perc of points you can steal by killing a hider.");
	// Hider shop items
	g_cvPointsHiderHeal = CreateConVar("prophunt_ranking_points_hider_heal", "3.0", "Bought bonus health.");
	g_cvPointsHiderFreezeHeightUpgrade = CreateConVar("prophunt_ranking_points_hider_freeze_height_upgrade", "10.0", "Bought freeze height upgrade.");
	g_cvPointsHiderSpeedUpgrade = CreateConVar("prophunt_ranking_points_hider_speed_upgrade", "10.0", "Bought speed upgrade.");
	g_cvPointsHiderLowGravityUpgrade = CreateConVar("prophunt_ranking_points_hider_low_gravity_upgrade", "10.0", "Bought low gravity upgrade.");
	g_cvPointsHiderPoisonousSmokeUpgrade = CreateConVar("prophunt_ranking_points_hider_poisonous_smoke_upgrade", "10.0", "Bought poisonous smoke upgrade.");
	g_cvPointsHiderMorph = CreateConVar("prophunt_ranking_points_hider_morph", "35.0", "Bought new random model.");
	g_cvPointsDecoy = CreateConVar("prophunt_ranking_points_decoy", "5.0", "Amount of points a player gets for placing a decoy.");
	g_cvPointsSmoke = CreateConVar("prophunt_ranking_points_smoke", "5.0", "Amount of points a player gets for using a smoke.");
	g_cvPointsUltimate1 = CreateConVar("prophunt_ranking_points_ultimate1", "1.0", "Amount of points a player gets for ultimate 1.");
	g_cvPointsUltimate2 = CreateConVar("prophunt_ranking_points_ultimate2", "25.0", "Amount of points a player gets for ultimate 2.");
	g_cvPointsUltimate3 = CreateConVar("prophunt_ranking_points_ultimate3", "42.0", "Amount of points a player gets for ultimate 3.");
	// Hider skills
	g_cvPointsHiderSmokeDmg = CreateConVar("prophunt_ranking_points_hider_smokedmg", "0.42", "Points per damage done by poisonous smoke.");
	g_cvPointsHiderDecoyDmg = CreateConVar("prophunt_ranking_points_hider_decoydmg", "0.42", "Points per damage done by decoy.");
	g_cvPointsHiderTauntTime = CreateConVar("prophunt_ranking_points_hider_taunttime", "0.42", "Points per second for hiders.");
	g_cvPointsHiderForceTauntTime = CreateConVar("prophunt_ranking_points_hider_forcetaunttime", "0.42", "Points per second for seekers.");
	// Seeker shop items
	g_cvPointsSeekerHealthshot = CreateConVar("prophunt_ranking_points_seeker_healthshot", "10.0", "Bought Healthshot.");
	g_cvPointsSeekerGrenade = CreateConVar("prophunt_ranking_points_seeker_grenade", "3.5", "Bought HE-Grenade.");
	g_cvPointsSeekerFiveSeven = CreateConVar("prophunt_ranking_points_seeker_fiveseven", "5.0", "Bought FiveSeven.");
	g_cvPointsSeekerXM1014 = CreateConVar("prophunt_ranking_points_seeker_xm1014", "6.0", "Bought XM1014.");
	g_cvPointsSeekerMP9 = CreateConVar("prophunt_ranking_points_seeker_mp9", "9.0", "Bought MP9.");
	g_cvPointsSeekerM4A1 = CreateConVar("prophunt_ranking_points_seeker_m4a1", "12.0", "Bought M4A1.");
	g_cvPointsSeekerAWP = CreateConVar("prophunt_ranking_points_seeker_awp", "25.0", "Bought AWP.");
	g_cvPointsSeekerHolyLightGrenade = CreateConVar("prophunt_ranking_points_seeker_holy_light_grenade", "15.0", "Bought holy light grenade.");
	g_cvPointsSeekerTauntGrenade = CreateConVar("prophunt_ranking_points_seeker_taunt_grenade", "5.0", "Bought taunt grenade.");
	
	AutoExecConfig(true, "prophunt-ranking");
	
	HookEvent("round_end", Event_RoundEnd);
	
	CreateTimer(3.0, Timer_CheckPoints, _ , TIMER_REPEAT);
	CreateTimer(90.0, Timer_CheckMinPlayers, _ , TIMER_REPEAT);
}

public void PH_OnHiderSpawn(int iClient)
{
	g_fPoints[iClient] = 0.0;
}

public void PH_OnSeekerSpawn(int iClient)
{
	g_fPoints[iClient] = 0.0;
}

public Action Event_RoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	if(!DEBUG && !MinPlayers())
		return Plugin_Continue;
	
	bool bHiderAlive;
	LoopAlivePlayers(iClient)
	{
		int iTeam = GetClientTeam(iClient);
		
		if(iTeam == CS_TEAM_CT)
			continue;
		
		bHiderAlive = true;
		break;
	}
	
	LoopAlivePlayers(iClient)
	{
		int iTeam = GetClientTeam(iClient);
		
		// Seeker
		if(iTeam == CS_TEAM_CT)
		{
			// Failed
			if(bHiderAlive)
			{
				Ranking_TakePoints(iClient, g_cvPointsSeekerFail.IntValue);
				CPrintToChat(iClient, "%sYou lost %i points.", CHAT_PREFIX, g_cvPointsSeekerFail.IntValue);
			}
			// Win
			else Ranking_AddPoints(iClient, g_cvPointsSeekerWin.IntValue);
		}
		//Hider
		else Ranking_AddPoints(iClient, g_cvPointsHiderSurvived.IntValue);
	}
	
	return Plugin_Continue;
}

public Action Timer_CheckPoints(Handle timer, any data)
{
	LoopIngamePlayers(iClient)
	{
		int iGive = RoundToFloor(g_fPoints[iClient]);
		if(iGive >= 1)
		{
			g_fPoints[iClient] -= float(iGive);
			Ranking_AddPoints(iClient, iGive);
		}
	}
	
	return Plugin_Continue;
}

public Action Timer_CheckMinPlayers(Handle timer, any data)
{
	int iCount;
	LoopIngamePlayers(i)
		iCount++;
	
	int iDiff = g_cvMinPlayers.IntValue - iCount;
	
	if(iDiff > 0)
		CPrintToChatAll("%sPoints system is disabled until %i more player%s join%s.", CHAT_PREFIX, iDiff, iDiff == 1 ? "" : "s", iDiff == 1 ? "s" : "");
	
	return Plugin_Continue;
}

/* Prophunt Events */

public void PH_OnBuyShopItemPost(int iClient, char[] sName, int iPoints)
{
	if(!DEBUG && !MinPlayers())
		return;
	
	if(StrEqual(sName, "Decoy"))
		g_fPoints[iClient] += g_cvPointsDecoy.FloatValue;
	else if(StrEqual(sName, "Smoke"))
		g_fPoints[iClient] += g_cvPointsSmoke.FloatValue;
	else if(StrEqual(sName, "Ultimate: Lvl. 1"))
		g_fPoints[iClient] += g_cvPointsUltimate1.FloatValue;
	else if(StrEqual(sName, "Ultimate: Lvl. 2"))
		g_fPoints[iClient] += g_cvPointsUltimate2.FloatValue;
	else if(StrEqual(sName, "Ultimate: Lvl. 3"))
		g_fPoints[iClient] += g_cvPointsUltimate3.FloatValue;
	else if(StrEqual(sName, "Heal"))
		g_fPoints[iClient] += g_cvPointsHiderHeal.FloatValue;
	else if(StrEqual(sName, "Change Model (Random)"))
		g_fPoints[iClient] += g_cvPointsHiderMorph.FloatValue;
	else if(StrEqual(sName, "Freeze Height Limit Upgrade"))
		g_fPoints[iClient] += g_cvPointsHiderFreezeHeightUpgrade.FloatValue;
	else if(StrEqual(sName, "Speed Upgrade"))
		g_fPoints[iClient] += g_cvPointsHiderSpeedUpgrade.FloatValue;
	else if(StrEqual(sName, "Low Gravity Upgrade"))
		g_fPoints[iClient] += g_cvPointsHiderLowGravityUpgrade.FloatValue;
	else if(StrEqual(sName, "Poisonous Smoke Upgrade"))
		g_fPoints[iClient] += g_cvPointsHiderPoisonousSmokeUpgrade.FloatValue;
	else if(StrEqual(sName, "Healthshot"))
		g_fPoints[iClient] += g_cvPointsSeekerHealthshot.FloatValue;
	else if(StrEqual(sName, "Grenade"))
		g_fPoints[iClient] += g_cvPointsSeekerGrenade.FloatValue;
	else if(StrEqual(sName, "FiveSeven"))
		g_fPoints[iClient] += g_cvPointsSeekerFiveSeven.FloatValue;
	else if(StrEqual(sName, "XM1014"))
		g_fPoints[iClient] += g_cvPointsSeekerXM1014.FloatValue;
	else if(StrEqual(sName, "MP9"))
		g_fPoints[iClient] += g_cvPointsSeekerMP9.FloatValue;
	else if(StrEqual(sName, "M4A1"))
		g_fPoints[iClient] += g_cvPointsSeekerM4A1.FloatValue;
	else if(StrEqual(sName, "AWP"))
		g_fPoints[iClient] += g_cvPointsSeekerAWP.FloatValue;
	else if(StrEqual(sName, "Holy Light Grenade"))
		g_fPoints[iClient] += g_cvPointsSeekerHolyLightGrenade.FloatValue;
	else if(StrEqual(sName, "Taunt Grenade"))
		g_fPoints[iClient] += g_cvPointsSeekerTauntGrenade.FloatValue;
}

public void PH_OnHiderHit(int hider, int seeker, int weapon, bool lethal, float &damage, int &bonus, int &take)
{
	if(!DEBUG && !MinPlayers())
		return;
	
	float fPoints = damage / g_cvPointsHiderHitDmgDiv.FloatValue;
	
	if(lethal && g_cvPointsHiderKill.FloatValue > fPoints)
		fPoints = g_cvPointsHiderKill.FloatValue;
	
	g_fPoints[seeker] += fPoints;
}

public void PH_OnTaunt(int hider, float soundln)
{
	if(!DEBUG && !MinPlayers())
		return;
	
	g_fPoints[hider] += g_cvPointsHiderTauntTime.FloatValue * soundln ;
}

public void PH_OnForceTaunt(int seeker, int hider, float soundln)
{
	if(!DEBUG && !MinPlayers())
		return;
	
	g_fPoints[hider] += g_cvPointsHiderTauntTime.FloatValue * soundln;
	g_fPoints[seeker] += g_cvPointsHiderForceTauntTime.FloatValue * soundln;
}

public void PH_OnHiderDeath(int hider, int seeker)
{
	if(!DEBUG && !MinPlayers())
		return;
	
	if(!ClientIsValid(hider))
		return;
	
	if(!ClientIsValid(seeker))
		return;
	
	float fPoints = float(Ranking_GetPoints(hider));
	int iTake = RoundToFloor(g_cvPointsHiderDeathMin.FloatValue + fPoints * (g_cvPointsHiderDeathPerc.FloatValue / 100.0));
	Ranking_TakePoints(hider, iTake);
	CPrintToChat(hider, "%sYou lost %i points.", CHAT_PREFIX, iTake);
	
	if(hider != seeker)
		g_fPoints[seeker] += fPoints * (g_cvPointsSeekerKillSteal.FloatValue / 100.0);
}

public void PH_OnSeekerDeath(int seeker, int hider)
{
	if(!DEBUG && !MinPlayers())
		return;
	
	if(!ClientIsValid(hider))
		return;
	
	if(!ClientIsValid(seeker))
		return;
	
	float fPoints = float(Ranking_GetPoints(seeker));
	int iTake = RoundToFloor(g_cvPointsSeekerDeathMin.FloatValue + fPoints * (g_cvPointsSeekerDeathPerc.FloatValue / 100.0));
	Ranking_TakePoints(seeker, iTake);
	CPrintToChat(seeker, "%sYou lost %i points.", CHAT_PREFIX, iTake);
	
	if(hider != seeker)
		g_fPoints[hider] += fPoints * (g_cvPointsHiderKillSteal.FloatValue / 100.0);
}

public void PH_OnSmokeHitSeeker(int iTarget, int iClient, int iDmg, float fDistance, int iTargetStack, int iEntity)
{
	if(!DEBUG && !MinPlayers())
		return;
	
	g_fPoints[iClient] += g_cvPointsHiderSmokeDmg.FloatValue * float(iDmg);
}

public void PH_OnDecoyHitSeeker(int iTarget, int iClient, int iDmg, float fDistance, int iEntity)
{
	if(!DEBUG && !MinPlayers())
		return;
	
	g_fPoints[iClient] += g_cvPointsHiderDecoyDmg.FloatValue * float(iDmg);
}

bool MinPlayers()
{
	int iCount;
	LoopIngamePlayers(i)
	{
		iCount++;
		
		if(iCount >= g_cvMinPlayers.IntValue)
			return true;
	}
	
	return false;
}

stock bool ClientIsValid(int iClient)
{
	return (0 < iClient <= MaxClients && IsClientInGame(iClient));
}